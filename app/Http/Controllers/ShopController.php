<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use Illuminate\Support\Facades\Auth;
use App\Category;
use App\Product;

class ShopController extends Controller {

  public function index() {
    // Get the categories with the related products
    $categories = Category::with('products')->get();
    return view('shop/index', compact('categories'));
  }

  public function category($id) {
    // go to the products of the category
    $products = Category::find($id)->products;
    return view('shop/product', compact('products'));
  }

  public function addToShoppingCart(Request $request) {
    // validate the input of the form
    $data = $this->validate($request, [
      'id' => 'required|integer|min:1|max:2147483647',
      'amount' => 'required|integer|min:1|max:100',
    ]);

    // get product by the id of the $data
    $product = Product::find($data['id']);

    // if there is no product then redirect the customer
    if (!$product) {
      return Redirect()->action('ShopController@viewCart');
    }

    // get the current cart
    $currentCart = $request->session()->get('cart');

    // if the current cart is not a array then add a empty array
    if (!is_array($currentCart)) {
      $currentCart = [];
    }

    // if the array key product id does not exist create it
    if (!array_key_exists($data['id'], $currentCart)){
      $currentCart[$data['id']] = [
        'name' => $product->name,
        'price' => $product->price,
        'amount' => $data['amount']
      ];
      // if the product id exist then add the amount
    }else{
      $currentCart[$data['id']]['amount'] += $data['amount'];
    }

    // put the current cart back in the session
    $request->session()->put('cart', $currentCart);
    return Redirect()->action('ShopController@viewCart');
  }

  //remove de cart session
  public function clear(Request $request) {
    // flush the cart session and redirect to the cart page
    $request->session()->forget('cart');
    return Redirect()->back();
  }

  private function cart(Request $request) {
    // get current cart
    $cart = $request->session()->get('cart');
    // return the cart view and check if the cart is empty
    return $cart ?: [];
  }

  public function viewCart(Request $request) {
    $cart = $this->cart($request);
    $total = $this->totalPrice($request);

    return view('shop/cart', [
      'cart' => $cart,
      'total' => $total,
    ]);
  }

  public function delete(Request $request, $id) {
    // get current cart
    $currentCart = $request->session()->get('cart');

    // if there is no array then redirect back
    if (!is_array($currentCart)) {
      return Redirect()->back();
    }

    // if the array exists then delete the product in the cart
    if (array_key_exists($id, $currentCart)) {
      unset($currentCart[$id]);
    }

    // put back the cart data and return to the cart page.
    $request->session()->put('cart', $currentCart);
    return Redirect()->action('ShopController@viewCart');
  }

  // calculate the total price of all products
  private function totalPrice(Request $request) {
    // get the cart from the session
    $cart = $request->session()->get('cart', []);
    // set total price on 0
    $total = 0;

    // foreach cart as item
    foreach ($cart as $item) {
      // sum up the total price with the outcome
      $total += ($item['price'] * $item['amount']);
    }
    // return the total price.
    return $total;
  }

  public function order(Request $request) {
    $cart = $request->session()->get('cart');
    // return the cart view and check if the cart is empty
    if($user = Auth::user('customer'))
    {
      return view('shop/order', [
        'cart' => (is_array($cart) ? $cart:[]),
      ]);
    }
  }
}
