<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;

use Illuminate\Http\Request;
use Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      $products = Product::all();
      return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
      $categories = Category::all()->pluck('name', 'id');
      return view ('products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
      $rules = [
        'name' => 'required|min:3',
        'description' => 'required|min:3',
        'price' => 'required|min:1|between:0,99.99',
        'amount' => 'required|integer|min:1|max:100',
      ];

      $validator = Validator::make($request->all(), $rules);

      if ($validator->fails()) {
        return redirect('products/create')
          ->withErrors($validator)
          ->withInput();
      } else {
        $product = new Product();
        $product->name=$request->input('name');
        $product->description=$request->input('description');
        $product->price=$request->input('price');
        $product->amount=$request->input('amount');

        $product->save();
        $product->categories()->sync($request->get('category_id'));
      }

      return redirect('products')->with('succes', 'Product has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product) {
      return view('products.show', ['product' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
      $categories = Category::all()->pluck('name', 'id');
      return view ('products.edit', ['product' => $product] , compact('categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
      $rules = array(
        'name' => 'required|min:3',
      );

      $validator = Validator::make($request->all(), $rules);

      if ($validator->fails()) {
        return redirect()
          ->action('ProductController@edit', ['id' => $product->id])
          ->withErrors($validator)
          ->withInput();
      } else {
        $product->name = $request->input('name');
        $product->description = $request->input('description');
        $product->price = $request->input('price');
        $product->amount = $request->input('amount');

        $product->save();
        $product->categories()->sync($request->get('category_id'));
      }

      return redirect('products')->with('succes', 'Product has been edited!');
    }

    /**
     * Remove the specified product from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product) {
      try {
        $product->delete();
        return redirect('products')->with('success', 'Product has been deleted!');
      } catch (\Illuminate\Database\QueryException $e) {
        return redirect('products')->withErrors('Failed to delete product, it still has a category attached.');
      }
    }
}
