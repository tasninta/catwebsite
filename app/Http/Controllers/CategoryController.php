<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;

use App\Category;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $categories = Category::with('products')->get();
      return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view ('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $rules = array(
        'name' => 'required|min:3',
      );

      $validator = Validator::make($request->all(), $rules);

      if ($validator->fails()) {
        return redirect('categories/create')
          ->withErrors($validator)
          ->withInput();
      } else {
        $category = new Category();
        $category->name = $request->input('name');
        $category->save();
      }

      return redirect('categories')->with('succes', 'Category has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('categories.show', ['category' => $category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
      return view('categories.edit', ['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
      $rules = array(
        'name' => 'required|min:3',
      );

      $validator = Validator::make($request->all(), $rules);

      if ($validator->fails()) {
        return redirect()
          ->action('CategoryController@edit', ['id' => $category->id])
          ->withErrors($validator)
          ->withInput();
      } else {
        $category->name = $request->input('name');
        $category->save();
      }

      return redirect('categories')->with('succes', 'Category has been edited!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category) {
      try {
        $category->delete();
        return redirect('categories')->with('success', 'Category has been deleted!');
      } catch (\Illuminate\Database\QueryException $e) {
        return redirect('categories')->withErrors('Failed to delete category, it still products attached.');
      }
    }
}
