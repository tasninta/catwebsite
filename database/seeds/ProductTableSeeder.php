<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\Category;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
      {
        $category_toys = Category::where('name', 'Speelgoed')->first();
        $category_litter  = Category::where('name', 'Kattenbakvulling')->first();
        $category_scratching  = Category::where('name', 'Krabpalen')->first();
        $category_food  = Category::where('name', 'Voer')->first();
        $category_snacks  = Category::where('name', 'Snacks')->first();

        $food = new Product();
        $food->name = 'Carocroc 10 kilo';
        $food->description = 'Carocroc is hoogwaardig voer. Het is bedoeld voor katten met gevoelige darmen';
        $food->price = '45';
        $food->amount = '50';
        $food->save();
        $food->categories()->attach($category_food);

        $litter = new Product();
        $litter->name = 'Peewee 9kg';
        $litter->description = 'Peewee is een kattenbakvulling op houtbasis';
        $litter->price = '18';
        $litter->amount = '20';
        $litter->save();
        $litter->categories()->attach($category_litter);

        $snacks = new Product();
        $snacks->name = 'Cosma Snackies Kip';
        $snacks->description = 'Gevriesdroogde snacks in kip smaak';
        $snacks->price = '4';
        $snacks->amount = '30';
        $snacks->save();
        $snacks->categories()->attach($category_snacks);

        $scratching = new Product();
        $scratching->name = 'Bamboe krabpaal';
        $scratching->description = 'Stevige krabpaal gemaakt van natuurlijk materiaal bamboe';
        $scratching->price = '124';
        $scratching->amount = '5';
        $scratching->save();
        $scratching->categories()->attach($category_scratching);

        $toys = new Product();
        $toys->name = 'Eenhoorn';
        $toys->description = 'Een eenhoorn speeltje met catnip erin';
        $toys->price = '6';
        $toys->amount = '40';
        $toys->save();
        $toys->categories()->attach($category_toys);
      }
}
