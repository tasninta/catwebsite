<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $category_toys = new Category();
    $category_toys->name = 'Speelgoed';
    $category_toys->save();

    $category_litter = new Category();
    $category_litter->name = 'Kattenbakvulling';
    $category_litter->save();

    $category_scratching = new Category();
    $category_scratching->name = 'Krabpalen';
    $category_scratching->save();

    $category_food = new Category();
    $category_food->name = 'Voer';
    $category_food->save();

    $category_snacks = new Category();
    $category_snacks->name = 'Snacks';
    $category_snacks->save();
  }
}
