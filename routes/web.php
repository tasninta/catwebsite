<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/shop', 'Shopcontroller@index')->name('shop');
Route::get('/categories/{id}', 'ShopController@category');
Route::post('/cart/add', 'ShopController@addToShoppingCart');
Route::get('/cart', 'ShopController@viewCart');
Route::get('/clear', 'ShopController@clear');
Route::get('/delete/{id}', 'ShopController@delete');
Route::get('/order', 'ShopController@order');


Route::resource('/products', 'ProductController');

Route::resource('/categories', 'CategoryController');

///**
// * All pages below requires to be a logged in user.
// */
//Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
//  Route::get('/products', 'ProductController@create');
//});
//
//Route::group(['prefix' => 'customer', 'middleware' => 'auth'], function () {
//  Route::get('/categories', 'CategoryController@index');
//});
