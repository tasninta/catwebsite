@extends('layouts.app')

@section('content')
    <nav class="nav">
        <a class="nav-link" href="{{ URL::to('products/create') }}">Add new product</a>
    </nav>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <div class="container">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Amount</th>
                <th>Category</th>
                <th colspan="2">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{$product['id']}}</td>
                    <td>{{$product['name']}}</td>
                    <td>{{$product['description']}}</td>
                    <td>€{{$product['price']}}</td>
                    <td>{{$product['amount']}}</td>
                    @foreach ($product->categories as $category)
                        <td>{{ $category->name }}</td>
                    @endforeach
                    <td><a href="{{action('ProductController@edit', ['id' => $product->id])}}" class="btn btn-warning">Edit</a></td>
                    <td>
                        <form action="{{action('ProductController@destroy', ['id' => $product->id])}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

