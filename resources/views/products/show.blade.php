@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{ $product->name }}</h1>
        <li>{{ $product->description }}</li>
        <li>€ {{ $product->price }}</li>
    </div>
@endsection
