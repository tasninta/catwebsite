@extends('layouts.app')

@section('content')
    <h2>Edit the product</h2>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container">
        <form method="post" action="{{ action('ProductController@update', $product->id) }}">
            @csrf
            @method('PUT')
            <input name="_method" type="hidden" value="PUT">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" value="{{$product->name}}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="description">Description:</label>
                    <input type="text" class="form-control" name="description" value="{{$product->description}}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="price">Price:</label>
                    <input type="number" class="form-control" name="price" value="{{$product->price}}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="amount">Amount:</label>
                    <input type="number" class="form-control" name="amount" value="{{$product->amount}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="category" class="col-sm-4 col-form-label text-md-right">Choose a category</label>
                <div class="col-md-6">
                    <select id="category" name="category_id[]" multiple="multiple">
                        @foreach($categories as $id => $name)
                            <option value="{{ $id }}">{{ $name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4" style="margin-top:60px">
                    <button type="submit" class="btn btn-success" style="margin-left:38px">Update</button>
                </div>
            </div>
        </form>
    </div>
@endsection
