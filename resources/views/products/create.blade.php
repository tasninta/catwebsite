@extends('layouts.app')

@section('content')

    <h1>Create a product</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- csrf is a type of misuse whereby the unauthorized commands are performed on behalf of an authenticated user.
 The csrf in this code is protecting it from the misuse.-->

    <form method="POST" action="{{ url('products') }}">
        @csrf
        <div class="form-group row">
            <label for="name" class="col-sm-4 col-form-label text-md-right">{{('name') }}</label>
            <div class="col-md-6">
                <input id="name" type="text" name="name" value="{{ old('name') }}">
            </div>
        </div>

        <div class="form-group row">
            <label for="description" class="col-sm-4 col-form-label text-md-right">{{('description') }}</label>
            <div class="col-md-6">
                <textarea rows="5" cols="20" id="description" type="text" name="description" value="{{ old('description') }}">
                </textarea>
            </div>
        </div>

        <div class="form-group row">
            <label for="price" class="col-sm-4 col-form-label text-md-right">{{('price') }}</label>
            <div class="col-md-6">
                <input id="price" type="number" step="any" name="price" value="{{ old('price') }}">
            </div>
        </div>

        <div class="form-group row">
            <label for="amount" class="col-sm-4 col-form-label text-md-right">{{('amount') }}</label>
            <div class="col-md-6">
                <input id="amount" type="number" name="amount" value="{{ old('amount') }}">
            </div>
        </div>

        <div class="form-group row">
            <label for="category" class="col-sm-4 col-form-label text-md-right">Choose a category</label>
            <div class="col-md-6">
                <select id="category" name="category_id[]" multiple="multiple">
                    @foreach($categories as $id => $name)
                        <option value="{{ $id }}">{{ $name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ ('Submit') }}
                </button>
            </div>
        </div>
    </form>
@endsection
