@extends('layouts.app')

@section('content')

<h1>Create a category</h1>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<!-- csrf is a type of misuse whereby the unauthorized commands are performed on behalf of an authenticated user.
 The csrf in this code is protecting it from the misuse.-->

<form method="POST" action="{{ url('categories') }}">
    @csrf
    <div class="form-group row">
        <label for="name" class="col-sm-4 col-form-label text-md-right">{{('name') }}</label>

        <div class="col-md-6">
            <input id="name" type="text" name="name" value="{{ old('name') }}">
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-8 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ ('Submit') }}
            </button>
        </div>
    </div>
</form>
@endsection
