@extends('layouts.app')

@section('content')
<h2>Edit the category</h2>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form method="post" action="{{ action('CategoryController@update', $category->id) }}">
    @csrf
    @method('PUT')

    <div class="row">
        <div class="col-md-4"></div>
        <div class="form-group col-md-4">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name" value="{{ old('name', $category->name) }}">
        </div>
    </div>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="form-group col-md-4" style="margin-top:60px">
            <button type="submit" class="btn btn-success" style="margin-left:38px">Update</button>
        </div>
    </div>
</form>
@endsection
