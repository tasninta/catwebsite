@extends('layouts.app')

@section('content')
    @if (session('succes'))
        <div class="alert alert-success">
            {{ session('succes') }}
        </div>
    @endif

    @foreach($categories as $category)
        <ul>
            <li><a href="/categories/{{$category['id']}}">{{$category['name']}}</a></li>
        </ul>
    @endforeach
@endsection

