@extends('layouts.app')

@section('content')
    @if(Session::has('id', 'amount'))
        <div class="alert alert-success">
            {{Session::get('id', 'amount')}}
        </div>
    @endif
    <div class="container">
        <h1>Categoriën:</h1>
        @foreach ($categories as $category)
            <li><a href="/categories/{{ $category->id }}">{{ $category->name }}</a></li>
        @endforeach
    </div>
@endsection
