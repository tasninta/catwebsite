@extends('layouts.app')

@section('content')
    <div class="container">
        @if (empty($cart))
            Er is nog niks in uw winkelwagen!
        @else
            <p>
                <a href="{{ action('ShopController@clear') }}">Winkelwagen legen</a>
                <a class="btn btn-success" href="{{ action('ShopController@order') }}">Bestelling plaatsen</a>
            </p>
        @endif
        @foreach ($cart as $id => $data)
            <h5>{{$data['name']}}</h5>
            <li>Hoeveelheid: {{$data['amount']}}</li>
            <li>Prijs: €{{$data['price']}}</li>
            <p>Totaalprijs {{$data['name']}}: € {{$data['price'] * $data['amount']}}</p>
            <a class="btn btn-danger" href="{{ action('ShopController@delete', [$id]) }}">Product verwijderen</a>
        @endforeach

        <p>Totaalprijs:€ {{$total}}</p>
    </div>
@endsection
