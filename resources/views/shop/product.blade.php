@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    @foreach ($products as $product)
        <div class="container">
            <li>{{ $product->name }}</li>
            <li>€ {{ $product->price }}</li>
            <li>{{ $product->amount }}</li>
            <li><a href="/products/{{ $product->id }}">Meer info</a></li>

            <form method="post" action="{{url('cart/add')}}">
                @csrf
                <label for="number">Aantal:</label>
                <input type="number" name="amount">
                <input type="hidden" id="id" name="id" value= {{$product->id }}>
                <button type="submit" class="btn btn-success">Add product</button>
            </form>
        </div>
    @endforeach
@endsection

